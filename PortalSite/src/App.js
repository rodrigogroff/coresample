import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';

import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Login } from './views/Login/Login';
import { Home } from './views/Home/Home';

export default class App extends Component 
{
    static displayName = App.name;
  
    render () {
      return (        
        <BrowserRouter>
            <Switch>  
                <Route exact path='/' component={Home} />
                <Route path='/login' component={Login} />
            </Switch>
        </BrowserRouter>
      );  
    }
  }
  