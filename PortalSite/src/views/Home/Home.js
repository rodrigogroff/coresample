import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import { Redirect } from 'react-router-dom';

import './Home.css';

export class Home extends Component {

    state = {
        logged: false,
        goLogin: false,
    }

    componentDidMount()
    {
        this.setState({ logged: localStorage.getItem('login') === 'true' });
    }

    gotoLogin = e =>
    {
        e.preventDefault();

        this.setState({ goLogin: true });
    }

    gotoSair = e =>
    {
        e.preventDefault();
        
        localStorage.setItem('login', null);
        this.setState({ logged: false });
    }

    render() 
    {
        if (this.state.goLogin === true)
        {
            return <Redirect to='/login' />
        }            
        else if (this.state.logged !== true)
        {
            return <Redirect to='/login' />
            /*
            return  <div className="App-header">
                        <h1>Home não logado</h1>   
                        <br></br>
                        <Button variant='info' id='btnLogin' value="login" onClick={this.gotoLogin}>Log In</Button>
                    </div>
                    */
        }
        else
        {
            return  <div className="App-header">
                        <h1>Home</h1>                
                        <Button variant='info' id='btnSair' onClick={this.gotoSair}>Sair</Button>
                    </div>
        }
    }
}
