import 'bootstrap/dist/css/bootstrap.min.css';
import './Login.css';

import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import InputMask from 'react-input-mask';

import Button from 'react-bootstrap/Button';
import Tab from 'react-bootstrap/Tab';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';

import { ApiLocation } from '../../shared/ApiLocation'

// user1@client.com
// 123456

export class Login extends Component {

    state = {
        email: '',
        password: '',
        error: '',
        loading: false,
        redirect: false,
        usuario_tke: '',
        password_tke: '',
    }

    executeLogin = e =>
    {
        e.preventDefault();

        console.log(localStorage);

        const Login = this.state.email;
        const Passwd = this.state.password;

        var loginData = JSON.stringify({ Login, Passwd });

        this.setState({ loading: true });
        this.setState({ error: '' });

        localStorage.setItem('login', null);

        fetch ( ApiLocation.api_host + ':' + ApiLocation.api_port + 
                ApiLocation.api_portal + 'authenticate', 
        { 
            method: 'POST', 
            headers: { 'Content-Type': 'application/json' }, 
            body: loginData
        })
        .then((res) => 
        {
            if (res.ok === true) {      
                localStorage.setItem('login', 'true');
                this.setState({ loading: false });         
                this.setState({ redirect: true });
            }
            else res.json().then((data) => {
                this.setState({ error: data.message });
                this.setState({ loading: false });
            });
        })
        .catch((errorMsg) => 
        {
            this.setState({ loading: false });
            this.setState({ error: errorMsg.toString() });
        });
    }

    executeLoginTKE = e =>
    {
        e.preventDefault();

        console.log(localStorage);

        const Login = this.state.email;
        const Passwd = this.state.password;

        var loginData = JSON.stringify({ Login, Passwd });

        this.setState({ loading: true });
        this.setState({ error: '' });

        localStorage.setItem('login', null);

        fetch ( ApiLocation.api_host + ':' + ApiLocation.api_port + 
                ApiLocation.api_portal + 'authenticate', 
        { 
            method: 'POST', 
            headers: { 'Content-Type': 'application/json' }, 
            body: loginData
        })
        .then((res) => 
        {
            if (res.ok === true) {      
                localStorage.setItem('login', 'true');
                this.setState({ loading: false });         
                this.setState({ redirect: true });
            }
            else res.json().then((data) => {
                this.setState({ error: data.message });
                this.setState({ loading: false });
            });
        })
        .catch((errorMsg) => 
        {
            this.setState({ loading: false });
            this.setState({ error: errorMsg.toString() });
        });
    }

    //https://react-bootstrap.github.io/components/tabs/

    //<input type="text" className="form-control"  onChange={event => this.setState({ email: event.target.value })} placeholder="CPF" />

    render() {

        // se login for com sucesso (home logado)
        if (this.state.redirect === true)
        {
            return <Redirect to='/' />
        }
        else
        {
            return  <div className="Login-header">
                        <div align='center' className='boxLogin'>                        
                            <h1 className="h1Login">Portal ON - Obras Novas</h1>
                            Forneça abaixo suas credenciais de acesso: <br></br>      
                            <br></br>
                            <br></br>
                            <Tab.Container defaultActiveKey="Cliente" className="tabContainerLogin">
                            <Row className="no-margin">
                                <Col sm={4}>
                                    <Nav variant="pills" className="flex-column">
                                        <Nav.Item>
                                            <Nav.Link eventKey="Cliente">Cliente</Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                            <Nav.Link eventKey="Colaborador">Colaborador</Nav.Link>
                                        </Nav.Item>
                                    </Nav>
                                </Col>
                                <Col sm={6}>
                                    <Tab.Content>
                                        <Tab.Pane eventKey="Cliente" className="rowLogin">
                                        <table> 
                                            <tbody><tr><td width='300px'></td></tr></tbody>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <InputMask mask='999.999.999-99' id="login" placeholder=" " type="text" onChange={event => this.setState({ email: event.target.value })} /><span>CPF</span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input id="password" placeholder=" " type="password" onChange={event => this.setState({ password: event.target.value })} /><span>Senha</span>
                                                    </label>                                                    
                                                </td>
                                            </tr>
                                            <tr height='20px'></tr>
                                            <tr>
                                                <td >
                                                    <Button className="btnArredondado" value="login" 
                                                    onClick={this.executeLogin}>Entrar</Button>
                                                </td>                                            
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br></br>
                                                    {
                                                        this.state.loading === true ? 
                                                        <div className="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div>
                                                            :
                                                        <label className="errorLabel">{this.state.error}</label>
                                                    }            
                                                </td>
                                            </tr>
                                        </table>   
                                    </Tab.Pane>
                                    <Tab.Pane eventKey="Colaborador" className="rowLogin">
                                        <table> 
                                            <tbody><tr><td width='300px'></td></tr></tbody>
                                            <tr>
                                                <td>
                                                    <label> 
                                                        <input id="login_tke" placeholder=" " type="text" onChange={event => this.setState({ usuario_tke: event.target.value })} /><span>Nome Usuário</span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <label>
                                                        <input id="password_tke" placeholder=" " type="password" onChange={event => this.setState({ password: event.target.value })} /><span>Senha</span>
                                                    </label>                                                    
                                                </td>
                                            </tr>
                                            <tr height='20px'></tr>
                                            <tr>
                                                <td >
                                                    <Button className="btnArredondado" value="login"
                                                     onClick={this.executeLoginTKE}>Entrar</Button>
                                                </td>                                            
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br></br>
                                                {
                                                    this.state.loading === true ? 
                                                    <div className="spinner-border text-info" role="status"><span class="sr-only">Loading...</span></div>
                                                        :
                                                    <label className="errorLabel">{this.state.error}</label>
                                                }            
                                                </td>
                                            </tr>
                                        </table> 
                                    </Tab.Pane>                                
                                </Tab.Content>
                            </Col>
                        </Row>
                    </Tab.Container>
                    <p>Versão 1.00</p>
                    <br></br>
                </div>
            </div>            
        }
    }
}
