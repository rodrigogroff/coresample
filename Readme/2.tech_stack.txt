	
---------------------------------
Tech Stack
---------------------------------

Web Server
	Asp.NET Core 2.2

User Interface
	Portal (react)
	Mobile (react-native)

Platform
	Mass setup (fake data)
	Crawler (rest simulation / stresser)
	Robot testing (selenium)

QA
	Unit testing
	Full regression tests
	Api Test (Insomnia environment)
	